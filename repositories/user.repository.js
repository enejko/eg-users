const fs = require('fs');
const usersMap = new Map(
    JSON.parse(fs.readFileSync('userlist.json', 'utf8'))
        .map(el => [el._id, el]))

//GET: /user/:id
//get one user by ID
const getUserInfo = (_id) => {
    if (usersMap.has(_id)) {
        return usersMap.get(_id);
    }
    return null;
}

//POST: /user
//create user according to the data from the request body
const postUser = (_id, attr) => {
    if (!_id || !attr) { return false; }
    //new users don't erase existent entries
    if (usersMap.has(_id)) {
        return false;
    }
    cu = usersMap.set(_id, attr);
    return true;
}

//PUT: /user/:id
//update user according to the data from the request body
const putUser = (_id, attr) => {
    if (!_id || !attr) { return false; }
    if (!usersMap.has(_id)) { return false; }

    cu = usersMap.get(_id);
    Object.keys(attr).forEach(key => cu[key] = attr[key]);
    //cu is already in Map...
    return true;
}

//DELETE: /user/:id
//delete one user by ID
const deleteUser = (_id) => {
    if (!_id) { return false; }
    if (!usersMap.has(_id)) { return false; }
    usersMap.delete(_id);
    return true;
}

//GET: /user
//get an array of all users
const getAllUsers = () => {
    all = [];
    usersMap.forEach(el => all.push(el));
    return all;
}

const DB = { getAllUsers, getUserInfo, postUser, putUser, deleteUser };

module.exports = { DB };