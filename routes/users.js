const express = require('express');
const router = express.Router();
const { getUsers } = require('../services/user.service');

//GET: /user  get an array of all users
router.get('/', function (req, res, next) {
  result = getUsers();
  if (result) {
    res.status(200).json(result);
  } else {
    res.status(400).send("absent data");
  }
});

const { createUser } = require('../services/user.service');
//POST: /user  create user according to the data from the request body
router.post('/', (req, res, next) => {
  if ((req.body instanceof Object) && req.body._id) {
    result = createUser(req.body._id, req.body);
    if (result) {
      res.send(`User ${req.body.name} is saved.`);
    }
  } else {
    res.status(400).send(`User with id=${req.body._id} already exist!`);
  }
})

// /user/:
const routerId = express.Router();
const { getUser } = require('../services/user.service');
const getId = (req) => {
  str = req.originalUrl;
  return str.substring(str.indexOf('/user/') + 6, str.length);
}
//GET: /user/:id  get one user by ID
routerId.get('/', (req, res, next) => {
  id = getId(req);
  if (id) {
    result = getUser(id);
    if (result) {
      res.status(200).json(result);
    }
  }
  res.status(400).send(`User with id=${id} is absent.`);
})

const { updateUser } = require('../services/user.service');
//PUT: /user/:id  update user according to the data from the request body
routerId.put('/', (req, res, next) => {
  id = getId(req);
  if (id) {
    result = updateUser(id, req.body);
    if (result) {
      res.status(200).send(`User with id=${id} is updated.`);
    }
  }
  res.status(400).send(`User with id=${id} is absent.`);
})


const { deleteUser } = require('../services/user.service');
//DELETE: /user/:id  delete one user by ID
routerId.delete('/', (req, res) => {
  id = getId(req);
  if (id) {
    result = deleteUser(id);
    if (result) {
      res.status(200).send(`User with id=${id} is deleted.`);
      return;
    }
  }
  res.status(400).send(`User with id=${id} is absent.`);
})
module.exports = { router, routerId };