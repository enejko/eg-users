const { DB } = require('../repositories/user.repository');

const getUsers = (id) => {
    au = DB.getAllUsers();
    if (au && au.length > 0) {
        return au;
    }
    return null;
}
const createUser = (_id, body) => {
    if (_id && body) {
        return DB.postUser(_id, body);
    }
}

const getUser = (id) => {
    if (id) {
        return DB.getUserInfo(id);
    }
    return null;
}

const updateUser = (id, body) => {
    if (id && body) {
        return DB.putUser(id, body);
    }
}

const deleteUser = (id) => {
    if (id) {
        return DB.deleteUser(id);
    }
    return null;
}
module.exports = { getUsers, createUser, getUser, updateUser, deleteUser };