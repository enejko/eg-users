const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const { router, routerId } = require('./routes/users');

const app = express();

app.use(compression());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/user/*', routerId);
app.use('/user', router);

module.exports = app;